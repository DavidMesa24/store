class OrdersController < ApplicationController
  before_action :set_order, only: %i[show destroy]

  def index
    @orders = current_user.Admin? ? Order.all : current_user.orders.group(:id)
    @total_amount = 0
  end

  def new
    @order = Order.new
  end

  def create
    @product = Product.find(params[:product_id])
    ord = Order.where('product_id = ?', @product.id).first
    @order = Order.new(order_params)
    if ord.nil?
      @order.product = @product
      @order.user = current_user
      if @order.quantity <= @product.stock
        @product.binnacle << ",PRODUCT STOCK SUBSTRACTION IN #{@order.quantity} BY USER #{current_user.id} AT #{Time.current} "
        @product.stock -= @order.quantity
      else
        @order.quantity = @product.stock
        @product.binnacle << ",PRODUCT STOCK UPDATE TO 0 BY USER #{current_user.id} AT #{Time.current} "
        @product.stock = 0
      end

    elsif @order.quantity <= @product.stock
      ord.quantity += @order.quantity
      @product.binnacle << ",PRODUCT STOCK SUBSTRACTION IN #{@order.quantity} BY USER #{current_user.id} AT #{Time.current} "
      @product.stock -= @order.quantity
      @order = ord
    else
      ord.quantity += @product.stock
      @product.binnacle << ",PRODUCT STOCK UPDATE TO 0 AT #{Time.current} "
      @product.stock = 0
      @order = ord

    end
    @product.save
    if @order.save
      redirect_to orders_path
    else
      render 'products/show'
    end
  end

  def destroy
    @order = Order.find(params[:id])
    unless params[:buy].present?
      pr = Product.find(@order.product_id)
      pr.binnacle << ",PRODUCT STOCK ADDED IN #{@order.quantity} BY USER#{current_user.id} AT #{Time.current} "
      pr.stock += @order.quantity
      pr.save
    end
    pr = Product.find(@order.product_id)
    pr.binnacle << ",PRODUCT BUY IN #{@order.quantity} BY USER#{current_user.id} AT #{Time.current} "
    @order.destroy
    redirect_to orders_path
  end

  private

  def set_order
    @order = Order.find(params[:id])
  end

  def order_params
    params.require(:order).permit(:quantity, :met)
  end
end
