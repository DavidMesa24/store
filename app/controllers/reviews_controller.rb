class ReviewsController < ApplicationController
  def create
    @product = Product.find(params[:product_id])
    @review = Review.new
    rev = Review.where("product_id = ? AND user_id = #{current_user.id}", params[:product_id]).first
    if rev.nil?
      @review.product = @product
      @review.user = current_user
      @review.score = if params[:commit] == 'Like'
                        @product.binnacle += ",PRODUCT LIKE BY USER #{current_user.id} "
                        @product.save!
                        @review.score.to_i + 1
                      else
                        @product.binnacle += ",PRODUCT DISLIKE BY USER #{current_user.id} "
                        @product.save!
                        @review.score.to_i - 1
                      end
    else
      @review = rev
    end

    if @review.save
      redirect_to product_path(@product)
    else
      render 'products/show'
    end
  end

  def new
    @review = Review.new
  end

  def index; end

  def destroy
    @review.destroy
    redirect_to product_path(params[:product_id])
  end

  private

  def review_params
    params.require(:product_id).permit(:commit)
  end
end
