class ProductsController < ApplicationController
  respond_to :html
  before_action :set_product, only: %i[show edit update destroy]
  before_action :authenticate_user!, only: %i[new create edit]

  def index
    @products = if params[:sort_by_name].present?
                  if params[:sort_by_name] == 'ASC'
                    Product.order('name ASC')
                  else
                    Product.order('name DESC')
                  end
                elsif params[:commit].present?
                  Product.where('name = ?', params[:query])
                else
                  Product.order('created_at DESC')
                end
  end

  def show
    @review = Review.new
    @order = Order.new
  end

  def new
    @product = Product.new
  end

  def create
    @product = Product.new(product_params)
    @product.binnacle = "CREATE AT #{Time.current} "
    if @product.save
      redirect_to products_path
    else
      render :new
    end
  end

  def edit
    @product = Product.find(params[:id])
    @product.binnacle += ",PRODUCT EDITED #{params[:id]} AT #{Time.current} "
  end

  def update
    @product = Product.find(params[:id])
    @product.binnacle += ",PRODUCT UPDATE [#{product_params}] AT #{Time.current} "
    if @product.update(product_params)
      redirect_to product_path(@product)
    else
      render :edit
    end
  end

  def destroy
    @product = Product.find(params[:id])
    @product.binnacle += ",PRODUCT DESTROY AT #{Time.current} "
    @product.destroy
    redirect_to products_path
  end

  private

  def set_product
    @product = Product.find(params[:id])
  end

  def product_params
    params.require(:product).permit(:sku, :name, :description, :price, :stock, :binnacle)
  end
end
