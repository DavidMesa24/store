class User < ApplicationRecord
  has_many :orders
  has_many :reviews
  enum rol: [:"User", :"Admin"]
  before_create :user_default
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
end

private
  def user_default
    self.rol = :"User"
  end