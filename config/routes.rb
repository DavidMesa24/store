Rails.application.routes.draw do
  devise_for :users
  devise_scope :user do
    get '/users/sign_out' => 'devise/sessions#destroy'
  end
  root to: 'products#index'
  resources :products do
    resources :orders
    resources :reviews
  end
  resources :orders, only: %i[index destroy buy]

  # admin

  namespace :admin do
    resources :products, only: %i[create update destroy]
  end
  resources :comments, only: %i[destroy index show] do
    patch 'approve', on: :member
  end
  resources :users, only: %i[create update destroy index show] do
  end

  # customer
namespace :customer do
  resources :products, only: [] do
    resource :review, only: %i[create destroy]
  end
  resources :orders, only: %i[index show]
  resources :users, only: [] do
    resources :comments, only: %i[create]
  end
  resources :comments, only: %i[index show]
  resource :cart, only: [:show] do
    resources :line_items, only: %i[update destroy] do
    end
    post 'checkout', on: :member
  end
end
end
